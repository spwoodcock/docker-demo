import sys
import logging
from pathlib import Path

import pandas as pd


logging.basicConfig(
    level="DEBUG",
    format=(
        "%(asctime)s.%(msecs)03d [%(levelname)s] "
        "%(name)s | %(funcName)s:%(lineno)d | %(message)s"
    ),
    datefmt="%y-%m-%d %H:%M:%S",
    stream=sys.stdout,
)

log = logging.getLogger(__name__)


url = "https://github.com/datablist/sample-csv-files/raw/main/files/customers/customers-100.csv"

log.info(f"Reading csv file into dataframe from url: {url}")
df = pd.read_csv(url)

log.info(f"Transposing dataframe {df}")
df = df.transpose()

filepath = Path(__file__).parent.resolve() / "output" / "processed.csv"
log.info(f"Deleting output file if already exists: {filepath}")
filepath.unlink(missing_ok=True)

log.info("Writing output csv to file")
df.to_csv(filepath)
